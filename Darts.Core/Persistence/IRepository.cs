﻿using System;
using System.Threading.Tasks;
using Darts.Domain.Infrastructure;

namespace Darts.Domain.Persistence
{
    public interface IRepository<T> where T : Aggregate
    {
        void Add(T agg);
        Task<T> Get(Identity aggregateId);
    }

    public class AggregateNotFoundException : Exception
    {
    }
}