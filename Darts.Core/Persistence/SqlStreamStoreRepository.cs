﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Darts.Common;
using Darts.Domain.Game.Events;
using Darts.Domain.Infrastructure;
using SqlStreamStore.Infrastructure;
using SqlStreamStore.Streams;

namespace Darts.Domain.Persistence
{
    public class SqlStreamStoreRepository<T> : IRepository<T> where T : Aggregate
    {
        private readonly StreamStoreBase _store;
        private readonly UnitOfWork _uow;
        private readonly Func<T> _factory;

        public SqlStreamStoreRepository(StreamStoreBase store, UnitOfWork uow, Func<T> factory)
        {
            _store = store;
            _uow = uow;
            _factory = factory;
        }

        public void Add(T agg)
        {
            _uow.Attach(agg);
        }

        public async Task<T> Get(Identity aggregateId)
        {
            var start = 0;
            const int pageSize = 100;
            var streamId = new StreamId(aggregateId.ToString());
            var page = await _store.ReadStreamForwards(streamId, start, pageSize);
            var events = new List<StreamMessage>(page.Messages);
            while (!page.IsEnd)
            {
                start += pageSize;
                page = await _store.ReadStreamForwards(streamId,
                    start,
                    pageSize);
                events.AddRange(page.Messages);
            }

            var loadEventDataTasks = events.Select(x => x.GetJsonData());
            var loadedEvents = await Task.WhenAll(loadEventDataTasks);
            if (!loadedEvents.Any())
                throw new AggregateNotFoundException();
            var domainEvents = loadedEvents.Select(e => Event.CreateFromJson(e, EventNames.TypeNameToType));
            var aggregate = _factory();
            aggregate.LoadEvents(domainEvents);
            _uow.Attach(aggregate);
            return aggregate;
        }
    }
}