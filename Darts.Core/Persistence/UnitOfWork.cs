﻿using System;
using System.Collections.Generic;
using System.Linq;
using Darts.Domain.Infrastructure;

namespace Darts.Domain.Persistence
{
    public class UnitOfWork
    {
        private readonly Dictionary<Identity, Aggregate> _tracked = new Dictionary<Identity, Aggregate>();

        public void Attach<T>(T agg) where T : Aggregate
        {
            if (_tracked.ContainsKey(agg.Id))
                throw new AlreadyTrackedException(agg.Id.ToString());

            _tracked[agg.Id] = agg;
        }

        public IEnumerable<Aggregate> GetChanges()
        {
            return _tracked.Values.Where(a => a.HasChanges());
        }

        public bool HasChanges()
        {
            return _tracked.Values.Any(a => a.HasChanges());
        }
    }

    public class AlreadyTrackedException : Exception
    {
        public readonly string Identifier;

        public AlreadyTrackedException(string identifier)
        {
            Identifier = identifier;
        }
    }
}