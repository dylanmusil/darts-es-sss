﻿using System;
using System.Collections.Generic;
using System.Linq;
using Darts.Domain.Infrastructure;

namespace Darts.Domain.Game.Entities
{
    public class Turn : ValueObject<Turn>
    {
        public Turn(Identity playerId, int ordinal, Throw first, Throw second, Throw third)
        {
            PlayerId = playerId;
            Ordinal = ordinal;
            First = first ?? throw new ArgumentNullException(nameof(first));

            // some turns can only have 1 throw
            Second = second;
            Third = third;
        }

        public Identity PlayerId { get; private set; }
        public int Ordinal { get; private set; }
        public Throw First { get; private set; }
        public Throw Second { get; private set; }
        public Throw Third { get; private set; }
        public int Score => First.Score + (Second?.Score ?? 0) + (Third?.Score ?? 0);
        private IEnumerable<Throw> Throws => new[] { First, Second, Third };
        public bool DoubledOut
        {
            get
            {
                if (Third != null)
                    return Third.Multiplier == 2;
                if (Second != null)
                    return Second.Multiplier == 2;
                return First.Multiplier == 2;
            }
        }

        public bool HasADouble => First.Multiplier == 2 ||
                                  (Second?.Multiplier ?? 0) == 2 ||
                                  (Third?.Multiplier ?? 0) == 2;

        protected override bool EqualsCore(Turn other)
        {
            return PlayerId == other.PlayerId && Ordinal == other.Ordinal &&
                   First == other.First && Second == other.Second &&
                   Third == other.Third;
        }

        protected override int GetHashCodeCore()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Allows the turn to be recorded without using the results of the turn in game scoring.
        /// For games like 301 and 501 a turn may not count if the player has busted.
        /// </summary>
        public void DoNotScore()
        {
            foreach (var @throw in Throws.Where(t => t != null))
            {
                @throw.VoidScore();
            }
        }

        public static Turn _180(Identity playerId, int ordinal) => new Turn(
            playerId,
            ordinal,
            new Throw(3, 20),
            new Throw(3, 20),
            new Throw(3, 20));

        /// <summary>
        /// The first turn that contains a double needs to have any darts
        /// voided that were thrown in the same turn but before the double.
        /// </summary>
        public void VoidScoresBeforeDouble()
        {
            if (!HasADouble)
                throw new NoDoubleFoundException("Use DoNotScore instead to ignore a turn's darts for scoring.");

            foreach (var @throw in Throws.Where(t => t != null))
            {
                if (@throw.Multiplier == 2)
                    break;

                @throw.VoidScore();
            }
        }
    }

    public class NoDoubleFoundException : Exception
    {
        public NoDoubleFoundException(string message) : base(message)
        {
        }
    }
}