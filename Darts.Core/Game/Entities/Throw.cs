﻿using System;
using Darts.Domain.Infrastructure;

namespace Darts.Domain.Game.Entities
{
    public class Throw : ValueObject<Throw>
    {
        public Throw(int multiplier, int value)
        {
            if (multiplier > 3 || multiplier < 0)
                throw new ArgumentOutOfRangeException(nameof(multiplier));
            if (value < 0 || value > 20 && value != 25)
                throw new ArgumentOutOfRangeException(nameof(value));

            Multiplier = multiplier;
            Value = value;
        }

        public int Value { get; }
        public int Multiplier { get; private set; }
        public int Score => ScoreVoided ? 0 : Value * Multiplier;
        public void VoidScore() => ScoreVoided = true;
        public bool ScoreVoided { get; private set; }

        protected override bool EqualsCore(Throw other)
        {
            return Multiplier == other.Multiplier && Value == other.Value;
        }

        protected override int GetHashCodeCore()
        {
            throw new NotImplementedException();
        }
    }
}