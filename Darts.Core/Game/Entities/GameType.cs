﻿namespace Darts.Domain.Game.Entities
{
    public enum GameType
    {
        Cricket,
        Single301,
        DoubleCricket,
        Single501,
        Double501,
        CutThroat,
        RealEstate
    }
}