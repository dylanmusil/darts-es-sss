﻿using System;
using System.Collections.Generic;

namespace Darts.Domain.Game.Commands
{
    public static class CommandNames
    {
        public const string AddPlayer = "AddPlayer";
        public const string CreateGame = "CreateGame";
        public const string SubmitTurn = "SubmitTurn";

        public static readonly Dictionary<string, Type> CommandNameToType =
            new Dictionary<string, Type>
            {
                {AddPlayer, typeof(AddPlayer)},
                {CreateGame, typeof(CreateGame)},
                {SubmitTurn, typeof(SubmitTurn)}
            };
    }
}