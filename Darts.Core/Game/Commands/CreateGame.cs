﻿using System;
using System.Threading.Tasks;
using Darts.Common;
using Darts.Domain.Game.Entities;
using Darts.Domain.Infrastructure;
using Darts.Domain.Persistence;

namespace Darts.Domain.Game.Commands
{
    public class CreateGame : Command
    {
        public Identity Identity { get; }
        public GameType GameType { get; }

        public CreateGame(Identity identity, GameType gameType)
        {
            Identity = identity;
            GameType = gameType;
        }

        public override string Name => CommandNames.CreateGame;
    }

    public class CreateGameHandler : IHandle<CreateGame>
    {
        private readonly IRepository<Game> _repository;
        private readonly Func<Game> _factory;

        public CreateGameHandler(IRepository<Game> repository, Func<Game> factory)
        {
            _repository = repository;
            _factory = factory;
        }

        public Task Handle(CreateGame command)
        {
            var game = _factory();
            game.Create(command.Identity, command.GameType);
            _repository.Add(game);
            return Task.CompletedTask;
        }
    }
}