﻿using System.Threading.Tasks;
using Darts.Common;
using Darts.Domain.Game.Entities;
using Darts.Domain.Game.Services;
using Darts.Domain.Infrastructure;
using Darts.Domain.Persistence;

namespace Darts.Domain.Game.Commands
{
    public class SubmitTurn : Command
    {
        public SubmitTurn(Identity gameId, Identity playerId, Turn turn)
        {
            GameId = gameId;
            PlayerId = playerId;
            Turn = turn;
        }

        public Identity GameId { get; }
        public Identity PlayerId { get; }
        public Turn Turn { get; }
        public override string Name => CommandNames.SubmitTurn;
    }

    public class SubmitTurnHandler : IHandle<SubmitTurn>
    {
        private readonly SqlStreamStoreRepository<Game> _repo;

        public SubmitTurnHandler(SqlStreamStoreRepository<Game> repo)
        {
            _repo = repo;
        }

        public async Task Handle(SubmitTurn command)
        {
            var game = await _repo.Get(command.GameId);
            game.AddTurnForPlayer(command.PlayerId,
                command.Turn,
                new SinglesTurnService(game),
                GameRules.CreateFor(game.GameType));
        }
    }
}