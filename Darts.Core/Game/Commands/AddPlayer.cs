﻿using System.Threading.Tasks;
using Darts.Common;
using Darts.Domain.Infrastructure;
using Darts.Domain.Persistence;

namespace Darts.Domain.Game.Commands
{
    public class AddPlayer : Command
    {
        public Identity GameId { get; private set; }
        public Identity PlayerId { get; private set; }

        public AddPlayer(Identity gameId, Identity playerId)
        {
            GameId = gameId;
            PlayerId = playerId;
        }

        public override string Name => CommandNames.AddPlayer;
    }

    public class AddPlayerHandler : IHandle<AddPlayer>
    {
        private readonly IRepository<Game> _repository;

        public AddPlayerHandler(IRepository<Game> repository)
        {
            _repository = repository;
        }

        public async Task Handle(AddPlayer command)
        {
            var game = await _repository.Get(command.GameId);
            game.AddPlayer(command.PlayerId);
        }
    }
}