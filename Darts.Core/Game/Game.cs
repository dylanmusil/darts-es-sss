﻿using System;
using System.Collections.Generic;
using System.Linq;
using Darts.Domain.Game.Entities;
using Darts.Domain.Game.Events;
using Darts.Domain.Game.Services;
using Darts.Domain.Infrastructure;

namespace Darts.Domain.Game
{
    public class Game : Aggregate
    {
        public static Game CreateNew() => new Game();

        private readonly HashSet<Identity> _players = new HashSet<Identity>();
        private readonly List<Turn> _turns = new List<Turn>();
        private bool _completed;
        private bool _started;

        public Game()
        {
            Register<GameCreated>(Apply);
            Register<PlayerAdded>(Apply);
            Register<TurnCompleted>(Apply);
            Register<GameCompleted>(Apply);
        }

        private void Apply(GameCompleted obj)
        {
            _completed = true;
        }

        private void Apply(TurnCompleted obj)
        {
            _started = true;
            _turns.Add(obj.Turn);
        }

        private void Apply(PlayerAdded obj)
        {
            _players.Add(obj.PlayerId);
        }

        private void Apply(GameCreated e)
        {
            Id = e.Id;
            GameType = e.GameType;
        }

        public void Create(Identity id, GameType type)
        {
            if (_completed)
                throw new GameAlreadyCompletedException();

            ApplyChange(new GameCreated(id, type, DateTime.UtcNow));
        }

        public void AddPlayer(Identity playerId)
        {
            if (_completed)
                throw new GameAlreadyCompletedException();
            if (_started)
                throw new GameAlreadyStartedException();
            if (_players.Contains(playerId))
                throw new CannotAddDuplicatePlayerException();

            ApplyChange(new PlayerAdded(Id, playerId));
        }

        public void AddTurnForPlayer(
            Identity playerId,
            Turn turn,
            IsItMyTurn turnService,
            IGameRules rules)
        {
            if (_completed)
                throw new GameAlreadyCompletedException();
            if (!_players.Contains(playerId))
                throw new PlayerNotPartOfGameException();
            if (!turnService.IsPlayersTurn(playerId, _turns.Select(t => t.PlayerId)))
                throw new NotThisPlayersTurnException();
            
            rules.ScoreTurn(turn, _turns);
            ApplyChange(new TurnCompleted(Id, playerId, turn));

            if (rules.IsGameCompleted(playerId, _turns))
                ApplyChange(new GameCompleted(Id, playerId));
        }

        public int PlayerCount => _players.Count;

        public GameType GameType { get; private set; }
    }

    public class PlayerNotPartOfGameException : Exception
    {
    }

    public class GameAlreadyStartedException : Exception
    {
    }

    public class GameAlreadyCompletedException : Exception
    {
    }

    public class NotThisPlayersTurnException : Exception
    {
    }

    public class CannotAddDuplicatePlayerException : Exception
    {
    }

  
}