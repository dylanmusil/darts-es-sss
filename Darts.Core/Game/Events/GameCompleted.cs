﻿using Darts.Common;
using Darts.Domain.Infrastructure;

namespace Darts.Domain.Game.Events
{
    public class GameCompleted : Event
    {
        public Identity Id { get; }
        public Identity Winner { get; }

        public GameCompleted(Identity id, Identity winner)
        {
            Id = id;
            Winner = winner;
        }

    }
}