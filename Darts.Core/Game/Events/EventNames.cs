﻿using System;
using System.Collections.Generic;

namespace Darts.Domain.Game.Events
{
    public static class EventNames
    {
        public const string GameCreated = "GameCreated";
        public const string PlayerAdded = "PlayerAdded";
        public const string GameCompleted = "GameCompleted";
        public const string TurnCompleted = "TurnCompleted";

        public static readonly Dictionary<string, Type> TypeNameToType =
            new Dictionary<string, Type>
            {
                {GameCreated, typeof(GameCreated)},
                {PlayerAdded, typeof(PlayerAdded)},
                {GameCompleted, typeof(GameCompleted)},
                {TurnCompleted, typeof(TurnCompleted)},
            };
    }
}