﻿using System;
using Darts.Common;
using Darts.Domain.Game.Entities;
using Darts.Domain.Infrastructure;

namespace Darts.Domain.Game.Events
{
    public class GameCreated : Event
    {
        public GameCreated(Identity id, GameType gameType, DateTime created)
        {
            Id = id;
            GameType = gameType;
            Created = created;
        }

        public readonly DateTime Created;
        public readonly Identity Id;
        public readonly GameType GameType;
    }
}