﻿using Darts.Common;
using Darts.Domain.Infrastructure;

namespace Darts.Domain.Game.Events
{
    public class PlayerAdded : Event
    {
        public readonly Identity Id;
        public readonly Identity PlayerId;

        public PlayerAdded(Identity id, Identity playerId)
        {
            Id = id;
            PlayerId = playerId;
        }

    }
}