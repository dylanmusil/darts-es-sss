﻿using Darts.Common;
using Darts.Domain.Game.Entities;
using Darts.Domain.Infrastructure;

namespace Darts.Domain.Game.Events
{
    public class TurnCompleted : Event
    {
        public Identity Id { get; }
        public Identity PlayerId { get; }
        public Turn Turn { get; }

        public TurnCompleted(Identity id, Identity playerId, Turn turn)
        {
            Id = id;
            PlayerId = playerId;
            Turn = turn;
        }

    }
}