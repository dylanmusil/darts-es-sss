﻿using System.Collections.Generic;
using System.Linq;
using Darts.Domain.Game.Entities;
using Darts.Domain.Infrastructure;

namespace Darts.Domain.Game.Services
{
    public interface IGameRules
    {
        bool IsGameCompleted(Identity playerId, IEnumerable<Turn> turns);
        void ScoreTurn(Turn newTurn, IEnumerable<Turn> existingTurns);
    }

    // ReSharper disable once InconsistentNaming
    public abstract class _01GameRules : IGameRules
    {
        protected readonly int WinningScore;

        protected _01GameRules(int winningScore)
        {
            WinningScore = winningScore;
        }

        /// <summary>
        /// Checks to see if the final dart for this turn resulted in a total score of 501 and was a double.
        /// </summary>
        /// <param name="playerId">The player whose turn is being submitted./</param>
        /// <param name="turns">The player's turns.</param>
        /// <returns>True if the player has won the game.</returns>
        public bool IsGameCompleted(Identity playerId, IEnumerable<Turn> turns)
        {
            var scoredTurns = turns
                .Where(t => t.PlayerId == playerId)
                .OrderBy(t => t.Ordinal)
                .ToList();

            var correctScore = scoredTurns.Sum(t => t.Score) == WinningScore;
            var doubledOut = scoredTurns.LastOrDefault()?.DoubledOut ?? false;
            return correctScore && doubledOut;
        }

        public abstract void ScoreTurn(Turn newTurn, IEnumerable<Turn> existingTurns);
        protected bool Busted(Turn newTurn, IEnumerable<Turn> existingTurns)
        {
            var newScore = existingTurns.Sum(t => t.Score) + newTurn.Score;
            return newScore >= WinningScore && !newTurn.DoubledOut || newScore == WinningScore - 1;
        }
    }

    // ReSharper disable once InconsistentNaming
    public class _501GameRules : _01GameRules
    {
        public _501GameRules() : base(501)
        {
        }

        /// <summary>
        /// 501 is "fly-in" and "double-out." The first dart always counts, and the final dart must be a double.
        /// If the turn being submitted results in the player busting, record it but do not count its score.
        /// </summary>
        /// <param name="newTurn">The turn being submitted.</param>
        /// <param name="existingTurns">Turns previously submitted.</param>
        public override void ScoreTurn(Turn newTurn, IEnumerable<Turn> existingTurns)
        {
            if (Busted(newTurn, existingTurns))
                newTurn.DoNotScore();
        }
    }

    // ReSharper disable once InconsistentNaming
    public class _301GameRules : _01GameRules
    {
        public _301GameRules() : base(301)
        {
        }

        /// <summary>
        /// 301 is "double-in" and "double-out."
        /// Darts do not start counting until a double is hit, and the final dart must be a double.
        /// If the turn being submitted results in the player busting, record it but do not count its score.
        /// If the turn submitted is the first turn or a double hasn't been seen in prior turns,
        /// the turn and any darts before the first double do not count.
        /// </summary>
        /// <param name="newTurn">The turn being submitted.</param>
        /// <param name="existingTurns">Turns previously submitted.</param>
        public override void ScoreTurn(Turn newTurn, IEnumerable<Turn> existingTurns)
        {
            var turnList = existingTurns.ToList();
            var scoringActive = turnList.Any(t => t.HasADouble);
            if (scoringActive)
            {
                if (Busted(newTurn, turnList))
                    newTurn.DoNotScore();
            }
            else
            {
                if (!newTurn.HasADouble)
                    newTurn.DoNotScore();
                else
                    newTurn.VoidScoresBeforeDouble();
            }
        }
    }
}