﻿using System;
using System.Collections.Generic;
using System.Linq;
using Darts.Domain.Infrastructure;

namespace Darts.Domain.Game.Services
{
    /// <summary>
    /// Determines if it is a certain player's turn. Used by a game to validate a turn submission.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public interface IsItMyTurn
    {
        bool IsPlayersTurn(Identity playerId, IEnumerable<Identity> turns);
    }

    /// <inheritdoc />
    /// <summary>
    /// Generic rules can be used to determine whose turn it is.
    /// <para>
    /// It's your turn if (total players - 1) turns have been taken since your last turn
    /// OR if fewer than (total players) turns have been taken.
    /// </para>
    /// The exception is when a team game is being played. Then teams must alternate.
    /// That case is handled by <see cref="T:Darts.Domain.Game.Services.TeamsTurnService" />
    /// </summary>
    public class SinglesTurnService : IsItMyTurn
    {
        private readonly int _playerCount;

        public SinglesTurnService(Game game)
        {
            _playerCount = game.PlayerCount;
        }

        public bool IsPlayersTurn(
            Identity playerId,
            IEnumerable<Identity> turns)
        {
            if (turns == null)
                throw new ArgumentNullException(nameof(turns));

            var turnList = turns.ToList();

            if (!turnList.Any() || turnList.Count < _playerCount)
                return turnList.All(t => t != playerId);

            // everyone has had at least one turn
            turnList.Reverse();
            // the last turn this player took was playerCount - 1 turns ago
            return turnList.Skip(_playerCount - 1)
                       .First() == playerId;
        }
    }

    public class TeamsTurnService : IsItMyTurn
    {
        public bool IsPlayersTurn(
            Identity playerId,
            IEnumerable<Identity> turns)
        {
            throw new NotImplementedException();
        }
    }
}