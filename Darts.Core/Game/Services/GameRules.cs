﻿using System;
using Darts.Domain.Game.Entities;

namespace Darts.Domain.Game.Services
{
    public static class GameRules
    {
        public static IGameRules CreateFor(GameType gameType)
        {
            switch (gameType)
            {
                default:
                    throw new NotImplementedException();
                case GameType.Single301:
                    return new _301GameRules();
                case GameType.Single501:
                    return new _501GameRules();
            }
        }
    }
}