﻿using System;
using System.Collections.Generic;
using System.Linq;
using Darts.Common;

namespace Darts.Domain.Infrastructure
{
    public abstract class Aggregate
    {
        public Identity Id { get; protected set; }
        private readonly List<Event> _events = new List<Event>();
        private readonly Router _router = new Router();
        public int Version { get; private set; } = -1;
        public void LoadEvents(IEnumerable<Event> events)
        {
            foreach (var @event in events)
            {
                _router.Apply(@event);
                Version++;
            }
        }

        protected void Register<T>(Action<T> handler) where T : Event
        {
            _router.Register(handler);
        }

        public IEnumerable<Event> GetUncommittedEvents()
        {
            return _events;
        }

        protected void ApplyChange(Event e)
        {
            _events.Add(e);
            _router.Apply(e);
        }

        public bool HasChanges()
        {
            return _events.Any();
        }
    }
}