﻿using System;
using System.Collections.Generic;
using Darts.Common;

namespace Darts.Domain.Infrastructure
{
    public class Router
    {
        private readonly Dictionary<Type, Action<Event>> _handlers =
            new Dictionary<Type, Action<Event>>();

        public void Apply(Event @event)
        {
            _handlers[@event.GetType()](@event);
        }

        public void Register<T>(Action<T> handler) where T : Event
        {
            if (_handlers.TryGetValue(typeof(T), out _))
            {
                throw new HandlerAlreadyRegisteredException();
            }

            _handlers[typeof(T)] = x => handler((T)x);
        }
    }
}