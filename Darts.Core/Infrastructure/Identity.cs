﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Darts.Domain.Infrastructure
{
    [JsonConverter(typeof(IdentityConverter))]
    public class Identity : ValueObject<Identity>
    {
        private readonly Guid _id;
        private Identity(Guid id)
        {
            if (id == Guid.Empty) throw new ArgumentException(nameof(id));
            _id = id;
        }

        protected override bool EqualsCore(Identity other)
        {
            return _id == other._id;
        }

        protected override int GetHashCodeCore()
        {
            return _id.GetHashCode();
        }

        public static implicit operator Guid(Identity i) => i._id;

        public static Identity Create()
        {
            return new Identity(Guid.NewGuid());
        }

        public static Identity From(Guid id) => new Identity(id);

        public override string ToString()
        {
            return _id.ToString();
        }
    }

    public class IdentityConverter : JsonConverter
    {
        public override void WriteJson(
            JsonWriter writer,
            object value,
            JsonSerializer serializer)
        {
            JToken.FromObject(value.ToString())
                .WriteTo(writer);
        }

        public override object ReadJson(
            JsonReader reader,
            Type objectType,
            object existingValue,
            JsonSerializer serializer)
        {
            var input = JToken.Load(reader)
                .ToString();
            return Identity.From(Guid.Parse(input));
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Identity);
        }
    }
}