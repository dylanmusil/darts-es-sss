﻿namespace Darts.Common
{
    public abstract class Command : Message
    {
        public abstract string Name { get; }
    }
}