﻿using System.Threading.Tasks;

namespace Darts.Common
{
    public interface IHandle<in T> where T : Command
    {
        Task Handle(T command);
    }
}