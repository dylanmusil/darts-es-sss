﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Darts.Common
{
    public class Mediator :
        ISendCommands,
        IRegisterCommandHandlers,
        IQuery,
        IRegisterQueryHandlers,
        INotifyOfEvents,
        IRegisterEventHandlers
    {
        private readonly Dictionary<Type, Func<Command, Task>> _commandHandlers =
            new Dictionary<Type, Func<Command, Task>>();

        /// <summary>
        /// Registers up to one command handler per command.
        /// </summary>
        /// <typeparam name="TCommand">The command type.</typeparam>
        /// <param name="handler">The function that handles the command.</param>
        public void Register<TCommand>(Func<TCommand, Task> handler) where TCommand : Command
        {
            if (_commandHandlers.TryGetValue(typeof(TCommand), out _))
            {
                throw new HandlerAlreadyRegisteredException();
            }

            _commandHandlers[typeof(TCommand)] = x => handler((TCommand)x);
        }

        /// <summary>
        /// Issues a command to its handler.
        /// </summary>
        /// <param name="c">The command.</param>
        /// <returns>The task to await.</returns>
        public Task Send(Command c)
        {
            return _commandHandlers[c.GetType()](c);
        }

        private readonly Dictionary<Type, Func<Query, Task<object>>> _queryHandlers =
            new Dictionary<Type, Func<Query, Task<object>>>();

        /// <summary>
        /// Finds and executes the handler for a query.
        /// </summary>
        /// <typeparam name="TRequest">The query type.</typeparam>
        /// <typeparam name="TResponse">The query response.</typeparam>
        /// <param name="query">The query object.</param>
        /// <returns>The results of the query.</returns>
        public async Task<TResponse> Query<TRequest, TResponse>(TRequest query) where TRequest : Query
        {
            return (TResponse)await _queryHandlers[typeof(TRequest)](query);
        }

        /// <summary>
        /// Registers up to one query handler for each query type.
        /// </summary>
        /// <typeparam name="TRequest">The query type.</typeparam>
        /// <typeparam name="TResponse">The query response.</typeparam>
        /// <param name="handler">The function that handles the query.</param>
        public void Register<TRequest, TResponse>(Func<TRequest, Task<TResponse>> handler) where TRequest : Query
        {
            if (_queryHandlers.TryGetValue(typeof(TRequest), out _))
            {
                throw new HandlerAlreadyRegisteredException();
            }

            _queryHandlers[typeof(TRequest)] = async q => await handler((TRequest)q);
        }

        private readonly Dictionary<Type, List<Func<Event, Task>>> _eventHandlers =
            new Dictionary<Type, List<Func<Event, Task>>>();

        /// <summary>
        /// Loads all the registered handlers for an event and runs them.
        /// </summary>
        /// <typeparam name="TEvent">The event type.</typeparam>
        /// <param name="event">The event to be processed.</param>
        /// <returns></returns>
        public async Task Notify<TEvent>(TEvent @event) where TEvent : Event
        {
            if (!_eventHandlers.TryGetValue(@event.GetType(), out var handlers))
            {
                return;
            }

            // with EF, I don't think there can be multiple
            // result sets/open transactions, so process
            // multiple handlers in sequence for the same event
            // TODO these should all run in a transaction and increment a version counter when done
            // TODO or perhaps that needs to be handled outside the mediator since it's only going
            // TODO to be happening during projection

            foreach (var h in handlers)
            {
                await h(@event);
            }
        }

        /// <summary>
        /// Registers event handlers. There can be multiple handlers for the same event.
        /// </summary>
        /// <typeparam name="TEvent">The event type</typeparam>
        /// <param name="handler">A function that handles the event and returns nothing.</param>
        void IRegisterEventHandlers.Register<TEvent>(Func<TEvent, Task> handler)
        {
            var type = typeof(TEvent);
            if (!_eventHandlers.TryGetValue(type, out var handlers))
            {
                handlers = new List<Func<Event, Task>>();
                _eventHandlers[type] = handlers;
            }

            handlers.Add(e => handler((TEvent)e));
        }
    }

    public interface IRegisterCommandHandlers
    {
        void Register<T>(Func<T, Task> handler) where T : Command;
    }

    public interface ISendCommands
    {
        Task Send(Command c);
    }

    public interface IRegisterQueryHandlers
    {
        void Register<TRequest, TResponse>(Func<TRequest, Task<TResponse>> handler) where TRequest : Query;
    }

    public interface IQuery
    {
        Task<TResponse> Query<TRequest, TResponse>(TRequest query) where TRequest : Query;
    }
    public interface IRegisterEventHandlers
    {
        void Register<TEvent>(Func<TEvent, Task> handler) where TEvent : Event;
    }

    public interface INotifyOfEvents
    {
        Task Notify<TEvent>(TEvent @event) where TEvent : Event;
    }

    

    public interface IQueryHandler<in TRequest, TResponse> where TRequest : Query
    {
        Task<TResponse> Handle(TRequest query);
    }

    public interface IEventHandler<in TRequest> where TRequest : Event
    {
        Task Handle(TRequest @event);
    }
    public class HandlerAlreadyRegisteredException : Exception
    {
    }
}