﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Darts.Common
{
    public abstract class Event : Message
    {
        public string Type => GetType()
            .Name;

        public static Event CreateFromJson(string json, Dictionary<string, Type> eventLookup)
        {
            var typeName = JObject.Parse(json)["Type"].ToString();
            if (eventLookup.TryGetValue(typeName, out var type))
            {
                return (Event) JsonConvert.DeserializeObject(json, type);
            }

            return null;
        }
    }

    
}