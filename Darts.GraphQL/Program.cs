﻿using System;
using GraphQL;
using GraphQL.Types;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Darts.GraphQL
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var schema = Schema.For(@"
            type Query {
                hello: String, 
                goodbye: String
            }");

            var json = schema.Execute(_ =>
            {
                _.Query = "{goodbye}";
                _.Root = new { hello = "Hello World!", goodbye = "So long" };
            });

            Console.WriteLine(json);
            Console.ReadLine();
            //CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
