﻿using System.Threading.Tasks;
using Darts.Common;
using Darts.Domain.Game.Commands;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Darts.Write.API.Controllers
{
    [Route("api/commands")]
    [ApiController]
    public class CommandsController : Controller
    {
        private readonly ISendCommands _commandBus;

        public CommandsController(ISendCommands commandBus)
        {
            _commandBus = commandBus;
        }

        [HttpPost]
        public async Task<IActionResult> Command([FromBody]JObject json)
        {
            var commandName = json["Name"].ToString();
            var c = (Command) JsonConvert.DeserializeObject(json.ToString(),
                CommandNames.CommandNameToType[commandName]);
            await _commandBus.Send(c);
            return Accepted();
        }
    }
}
