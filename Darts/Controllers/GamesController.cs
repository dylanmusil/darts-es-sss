﻿using System;
using System.Threading.Tasks;
using Darts.Domain.Game;
using Darts.Domain.Infrastructure;
using Darts.Domain.Persistence;
using Microsoft.AspNetCore.Mvc;

namespace Darts.Write.API.Controllers
{
    [Route("api/games/{id}")]
    public class GamesController
    {
        private readonly SqlStreamStoreRepository<Game> _games;

        public GamesController(SqlStreamStoreRepository<Game> games)
        {
            _games = games;
        }

        public Task<Game> GetGame(string id)
        {
            var identity = Identity.From(Guid.Parse(id));
            return _games.Get(identity);
        }
    }
}