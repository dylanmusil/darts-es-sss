﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Darts.Common;
using Darts.Domain.Game;
using Darts.Domain.Game.Commands;
using Darts.Domain.Persistence;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using SqlStreamStore;
using SqlStreamStore.Infrastructure;
using SqlStreamStore.Streams;

namespace Darts.Write.API.Infrastructure
{
    public static class BootstrappingExtensions
    {
        public static IServiceCollection RegisterCommandHandlers(
            this IServiceCollection services,
            string connectionString = null,
            bool isIntegration = true)
        {
            if (isIntegration)
            {
                // add the in-memory store when testing
                services.AddSingleton<StreamStoreBase, InMemoryStreamStore>();
            }
            else
            {
                if (connectionString == null)
                    throw new ArgumentNullException(nameof(connectionString));

                services.AddSingleton<StreamStoreBase, MsSqlStreamStore>(
                    provider =>
                    {
                        var settings =
                            new MsSqlStreamStoreSettings(connectionString);
                        var store = new MsSqlStreamStore(settings);
                        store.CreateSchema()
                            .GetAwaiter()
                            .GetResult();
                        return store;
                    });
            }

            services.AddScoped<UnitOfWork>();
            services.AddScoped(o =>
                new SqlStreamStoreRepository<Game>(
                    o.GetService<StreamStoreBase>(),
                    o.GetService<UnitOfWork>(),
                    Game.CreateNew));
            services.AddScoped<ISendCommands>(provider =>
            {
                var mediator = provider.GetService<Mediator>();
                var streamStore =
                    provider.GetService<StreamStoreBase>();
                var uow = provider.GetService<UnitOfWork>();
                var repo =
                    provider.GetService<SqlStreamStoreRepository<Game>>();
                mediator.Register<CreateGame>(command =>
                    new CreateGameHandler(repo, Game.CreateNew).WithPersistence(
                        command,
                        uow,
                        streamStore));
                mediator.Register<AddPlayer>(command =>
                    new AddPlayerHandler(repo).WithPersistence(command,
                        uow,
                        streamStore));
                mediator.Register<SubmitTurn>(command =>
                    new SubmitTurnHandler(repo).WithPersistence(command,
                        uow,
                        streamStore));
                return mediator;
            });

            return services;
        }
    }


    public class PersistenceDecorator<T, TCommand> : IDecorate<T, TCommand> 
        where T : IHandle<TCommand>
        where TCommand : Command
    {
        private readonly UnitOfWork _uow;
        private readonly StreamStoreBase _store;

        public PersistenceDecorator(UnitOfWork uow, StreamStoreBase store)
        {
            _uow = uow;
            _store = store;
        }

        public async Task Decorate(T handler, TCommand command)
        {
            await handler.Handle(command);
            if (_uow.HasChanges())
            {
                var changes = _uow.GetChanges();
                foreach (var aggregate in changes)
                {
                    var streamId = new StreamId(aggregate.Id.ToString());
                    var newStreamMessages = aggregate.GetUncommittedEvents()
                        .Select(e => new NewStreamMessage(Guid.NewGuid(),
                            e.Type,
                            JsonConvert.SerializeObject(e)))
                        .ToArray();

                    await _store.AppendToStream(
                        streamId,
                        aggregate.Version,
                        newStreamMessages);
                }
            }
        }
    }

    public interface IDecorate<in T, in TCommand> where T : IHandle<TCommand> where TCommand : Command
    {
        Task Decorate(T handler, TCommand command);
    }

    public static class PersistenceDecoratorExtensions
    {
        public static Task WithPersistence<T, TCommand>(
            this T handler,
            TCommand command,
            UnitOfWork uow,
            StreamStoreBase store)
            where T : IHandle<TCommand>
            where TCommand : Command
        {
            return new PersistenceDecorator<T, TCommand>(uow, store).Decorate(
                handler,
                command);
        }
    }
}