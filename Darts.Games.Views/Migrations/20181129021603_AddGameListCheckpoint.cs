﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Darts.Games.Views.Migrations
{
    public partial class AddGameListCheckpoint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GameListCheckpoint",
                columns: table => new
                {
                    Key = table.Column<byte>(nullable: false),
                    Version = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameListCheckpoint", x => x.Key);
                });
            migrationBuilder.Sql(
                "insert into GameListCheckpoint values (1, 0)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GameListCheckpoint");
        }
    }
}
