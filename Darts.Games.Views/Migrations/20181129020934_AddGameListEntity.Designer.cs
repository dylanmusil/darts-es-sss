﻿// <auto-generated />
using System;
using Darts.Games.Views;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Darts.Games.Views.Migrations
{
    [DbContext(typeof(DartsViewContext))]
    [Migration("20181129020934_AddGameListEntity")]
    partial class AddGameListEntity
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Darts.Games.Views.Game", b =>
                {
                    b.Property<int>("InternalId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("GameStatus");

                    b.Property<string>("GameType")
                        .HasMaxLength(25);

                    b.Property<Guid>("Identifier");

                    b.Property<Guid>("PlayerId");

                    b.HasKey("InternalId");

                    b.ToTable("Games");
                });
#pragma warning restore 612, 618
        }
    }
}
