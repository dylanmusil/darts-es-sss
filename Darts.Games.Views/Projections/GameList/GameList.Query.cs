﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Darts.Common;
using Darts.Domain.Infrastructure;

namespace Darts.Games.Views.Projections.GameList
{
    public class GameListQueries
    {
        public class ForPlayer : Query
        {
            public Identity PlayerId { get; }

            public ForPlayer(Identity playerId)
            {
                PlayerId = playerId;
            }
        }

        public class ForPlayerHandler : IQueryHandler<ForPlayer, GameListResult>
        {
            private readonly IGameListRepository _repo;

            public ForPlayerHandler(IGameListRepository repo)
            {
                _repo = repo;
            }

            public Task<GameListResult> Handle(ForPlayer query)
            {
                return _repo.GetGamesForPlayer(query.PlayerId);
            }
        }
    }

    public interface IGameListRepository
    {
        Task<GameListResult> GetGamesForPlayer(Identity playerId);
    }


    public class GameListResult
    {
        public List<Game> Games { get; }

        public GameListResult(List<Game> games)
        {
            Games = games;
        }
    }
}