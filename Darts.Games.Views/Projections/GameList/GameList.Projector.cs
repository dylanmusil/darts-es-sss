﻿using System;
using System.Threading.Tasks;
using Darts.Common;
using Darts.Domain.Infrastructure;

namespace Darts.Games.Views.Projections.GameList
{
    public enum GameType
    {
        Cricket,
        Single301,
        DoubleCricket,
        Single501,
        Double501,
        CutThroat,
        RealEstate
    }
    public class GameCreated : Event
    {
        public GameCreated(Identity id, GameType gameType, DateTime created)
        {
            Id = id;
            GameType = gameType;
            Created = created;
        }

        public readonly DateTime Created;
        public readonly Identity Id;
        public readonly GameType GameType;
    }

    public class GameCreatedHandler : IEventHandler<GameCreated>
    {
        private readonly DartsViewContext _db;

        public GameCreatedHandler(DartsViewContext db)
        {
            _db = db;
        }

        public Task Handle(GameCreated @event)
        {
            _db.AddGameDetail(new GameDetail(@event.Id, @event.GameType.ToString(), @event.Created));
            return Task.CompletedTask;
        }
    }

    public class PlayerAdded : Event
    {
        public readonly Identity Id;
        public readonly Identity PlayerId;

        public PlayerAdded(Identity id, Identity playerId)
        {
            Id = id;
            PlayerId = playerId;
        }

    }

    public class PlayerAddedHandler : IEventHandler<PlayerAdded>
    {
        private readonly DartsViewContext _db;

        public PlayerAddedHandler(DartsViewContext db)
        {
            _db = db;
        }

        public Task Handle(PlayerAdded @event)
        {
            _db.AddGame(new Game(@event.Id, @event.PlayerId));
            return Task.CompletedTask;
        }
    }
}