﻿using System;
using System.Collections.Generic;
using Darts.Games.Views.Projections.GameList;

namespace Darts.Games.Views.Projections
{
    public static class ReadEventNames
    {
        public const string GameCreated = "GameCreated";
        public const string PlayerAdded = "PlayerAdded";
        public const string GameCompleted = "GameCompleted";
        public const string TurnCompleted = "TurnCompleted";

        public static readonly Dictionary<string, Type> TypeNameToType =
            new Dictionary<string, Type>
            {
                {GameCreated, typeof(GameCreated)},
                {PlayerAdded, typeof(PlayerAdded)}
            };
    }
}