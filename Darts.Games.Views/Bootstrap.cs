﻿using Darts.Common;
using Darts.Games.Views.Projections.GameList;
using Microsoft.Extensions.DependencyInjection;

namespace Darts.Games.Views
{
    public static class BootstrappingExtensions
    {
        public static IServiceCollection RegisterReadHandlers(
            this IServiceCollection services,
            bool isIntegration = false)
        {
            services
                .AddScoped(o =>
                    new ContextFactory().CreateDbContext(new[] { isIntegration.ToString() }));
            services.AddScoped<IGameListRepository, DartsViewContext>(o =>
            {
                return o.GetService<DartsViewContext>();
            });
            services.AddScoped<IQuery>(provider =>
            {
                var mediator = provider.GetService<Mediator>();
                var repo =
                    provider.GetService<IGameListRepository>();
                mediator.Register<GameListQueries.ForPlayer, GameListResult>(
                    q => new GameListQueries.ForPlayerHandler(repo).Handle(q));

                return mediator;
            });
            services.AddScoped<INotifyOfEvents>(provider =>
                {
                    var mediator = provider.GetService<Mediator>();
                    var registrar = (IRegisterEventHandlers)mediator;
                    registrar.Register<GameCreated>(
                        q => new GameCreatedHandler(provider.GetService<DartsViewContext>()).Handle(q));
                    registrar.Register<PlayerAdded>(
                        q => new PlayerAddedHandler(provider.GetService<DartsViewContext>()).Handle(q));
                    return mediator;
                });

            return services;
        }
    }
}