﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Darts.Domain.Infrastructure;
using Darts.Games.Views.Projections.GameList;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Darts.Games.Views
{
    public class DartsViewContext : DbContext, IGameListRepository
    {
        public DartsViewContext(DbContextOptions options) : base(options)
        {
        }

        private DbSet<GameDetail> Games { get; set; }

        private DbSet<Game> GamesByPlayer { get; set; }
        private DbSet<GameCheckpoint> GameListCheckpoint { get; set; }

        private async Task UpdateGameListCheckpoint(int value)
        {
            var checkpoint = await GameListCheckpoint.SingleAsync();
            checkpoint.Version = value;
        }
        
        public void AddGame(Game game)
        {
            GamesByPlayer.Add(game);
        }

        public Task Reset()
        {
            GamesByPlayer.RemoveRange(GamesByPlayer);
            return UpdateGameListCheckpoint(0);
        }

        public async Task<GameListResult> GetGamesForPlayer(Identity playerId)
        {
            return new GameListResult(await GamesByPlayer.Where(g => g.PlayerId == playerId).ToListAsync());
        }

        public void AddGameDetail(GameDetail gameDetail)
        {
            Games.Add(gameDetail);
        }
    }

    public class GameDetail
    {
        public GameDetail(Guid gameId, string gameType, DateTime created)
        {
            GameId = gameId;
            GameType = gameType;
            Created = created;
        }
        [Key]
        public int InternalId { get; set; }
        public Guid GameId { get; set; }
        public DateTime Created { get; set; }
        [MaxLength(25)]
        public string GameType { get; set; }
        public GameStatus GameStatus { get; set; }
    }

    public class ContextFactory : IDesignTimeDbContextFactory<DartsViewContext>
    {
        public DartsViewContext CreateDbContext(string[] args)
        {
            const int integrationKey = 0;
            var builder = new DbContextOptionsBuilder();
            if (args.Length > integrationKey && bool.Parse(args[integrationKey]))
            {
                builder.UseInMemoryDatabase("DartGameViews");
            }
            else
            {

                builder.UseSqlServer(
                    "Data source=.;Initial catalog=Darts.Games.Views;Integrated security=true;");
            }
            return new DartsViewContext(builder.Options);
        }
    }

    /// <summary>
    /// Denormalized view of a game from a single player's perspective.
    /// </summary>
    public class Game
    {
        public Game(Guid identifier, Guid playerId)
        {
            Identifier = identifier;
            PlayerId = playerId;
        }

        [Key]
        public int InternalId { get; set; }
        public Guid Identifier { get; set; }
        public Guid PlayerId { get; set; }
        
    }

    /// <summary>
    /// Tracks what event the game list view processed last.
    /// </summary>
    public class GameCheckpoint
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public byte Key { get; set; } = 1;
        public int Version { get; set; }
    }

    public enum GameStatus
    {
        New,
        InProgress,
        Completed
    }
}
