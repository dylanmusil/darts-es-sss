﻿using System.Collections.Generic;
using Darts.Domain.Game.Entities;
using Darts.Domain.Game.Services;
using Darts.Domain.Infrastructure;
using Xunit;

namespace Darts.Domain.Tests
{
    // ReSharper disable once InconsistentNaming
    public class _501GameRulesTests
    {
        private readonly List<Turn> _turns;
        private readonly Identity _identity;
        private readonly _501GameRules _rules;

        public _501GameRulesTests()
        {
            _identity = Identity.Create();
            _turns = new List<Turn>
            {
                Turn._180(_identity, 1),
                Turn._180(_identity, 2)
            };
            _rules = new _501GameRules();
        }

        [Fact]
        public void FinalDartBusts_GameNotComplete()
        {
            _turns.Add(Turn._180(_identity, 3));
            Assert.False(_rules.IsGameCompleted(_identity, _turns));
        }

        [Fact]
        public void FinalDartFinishesGame_GameIsComplete()
        {
            // two 180s requires a 141 out shot, 60 57 24
            _turns.Add(new Turn(_identity,
                3,
                new Throw(3, 20),
                new Throw(3, 19),
                new Throw(2, 12)));
            Assert.True(_rules.IsGameCompleted(_identity, _turns));
        }

        [Fact]
        public void FinalDartMakes501ButIsNotDouble_GameNotComplete()
        {
            // two 180s requires a 141 out shot, 60 60 21
            _turns.Add(new Turn(_identity,
                3,
                new Throw(3, 20),
                new Throw(3, 20),
                new Throw(3, 7)));
            Assert.False(_rules.IsGameCompleted(_identity, _turns));
        }

        [Fact]
        public void TurnCausesBust_ScoreShouldNotCount()
        {
            var newTurn = Turn._180(_identity, 3);

            _rules.ScoreTurn(newTurn, _turns);

            Assert.Equal(0, newTurn.Score);
        }

        [Fact]
        public void BustThenDoubleOut_ShouldCompleteGame()
        {
            void ProcessNewTurn(Turn turn)
            {
                _rules.ScoreTurn(turn, _turns);
                _turns.Add(turn);
            }

            // bust and record it
            ProcessNewTurn(Turn._180(_identity, 3));

            // get the 141 with a double out
            ProcessNewTurn(new Turn(_identity,
                4,
                new Throw(3, 20),
                new Throw(3, 19),
                new Throw(2, 12)));

            // game should be completed
            Assert.True(_rules.IsGameCompleted(_identity, _turns));
        }

        [Fact]
        public void TurnDoesNotCauseBust_ScoreShouldCount()
        {
            var newTurn = new Turn(_identity,
                3,
                new Throw(3, 20),
                new Throw(3, 20),
                new Throw(2, 7));

            _rules.ScoreTurn(newTurn, _turns);

            Assert.Equal(134, newTurn.Score);
        }
    }
}