﻿using System;
using System.Collections;
using System.Linq;
using Darts.Common;
using Xunit;

namespace Darts.Domain.Tests
{
    public static class ListAssertions
    {
        public static void HasSingleOf<T>(this IEnumerable list) where T : Event
        {
            var objects = list.Cast<object>().ToArray();
            if (objects.Length != 1 || !(objects[0] is T))
            {
                throw new Exception();
            }
        }

        public static void Has<T>(this IEnumerable list, int expected) where T : Event
        {
            var count = list.OfType<T>().Count();

            Assert.Equal(expected, count);
        }
    }
}