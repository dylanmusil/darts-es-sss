﻿using System;
using System.Collections.Generic;
using Darts.Common;
using Darts.Domain.Game;
using Darts.Domain.Game.Commands;
using Darts.Domain.Game.Entities;
using Darts.Domain.Game.Events;
using Darts.Domain.Infrastructure;
using Xunit;

namespace Darts.Domain.Tests
{
    public class CannotAddPlayerToGameTwice : Specification<Game.Game, AddPlayer>
    {
        private readonly Identity _playerId = Identity.Create();

        protected override Game.Game CreateSubject()
        {
            return Game.Game.CreateNew();
        }

        protected override IEnumerable<Event> Given()
        {
            var identity = Identity.Create();
            yield return new GameCreated(identity,
                GameType.Cricket,
                DateTime.UtcNow);
            yield return new PlayerAdded(Subject.Id, _playerId);
        }

        protected override IHandle<AddPlayer> OnHandler()
        {
            return new AddPlayerHandler(new FakeRepository<Game.Game> { Subject });
        }

        protected override AddPlayer When()
        {
            return new AddPlayer(Subject.Id, _playerId);
        }

        [Fact]
        public void PlayerWasNotAdded()
        {
            Assert.IsType<CannotAddDuplicatePlayerException>(Caught);
        }
    }
}