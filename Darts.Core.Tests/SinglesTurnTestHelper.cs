﻿using System.Collections.Generic;
using Darts.Domain.Game.Services;
using Darts.Domain.Infrastructure;
using Xunit;

namespace Darts.Domain.Tests
{
    public class SinglesTurnTestHelper
    {
        public SinglesTurnTestHelper(int playerCount)
        {
            Game = Domain.Game.Game.CreateNew();
            for (var i = 0; i < playerCount; i++)
            {
                var playerId = Identity.Create();
                Game.AddPlayer(playerId);
                _players.Add(playerId);
            }

            Service = new SinglesTurnService(Game);
        }

        private readonly List<Identity> _players = new List<Identity>();

        public SinglesTurnService Service { get; }
        public Identity[] Players => _players.ToArray();
        public Game.Game Game { get; }
        public List<Identity> Turns { get; private set; } = new List<Identity>();
        public void PlayerTakesTurn(Identity player) => Turns.Add(player);
    }

    public class TurnServiceTests
    {
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        [InlineData(5)]
        [InlineData(6)]
        public void TurnOrderIsEnforced(int playerCount)
        {
            var helper = new SinglesTurnTestHelper(playerCount);

            for (var i = 0; i < 25; i++)
            {
                var isTurn =
                    helper.Service.IsPlayersTurn(helper.Players[0],
                        helper.Turns);
                Assert.True(isTurn || i % playerCount != 0);
                helper.PlayerTakesTurn(helper.Players[i % playerCount]);
            }
        }
    }
}