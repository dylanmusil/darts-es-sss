﻿using System;
using System.Collections.Generic;
using Darts.Common;
using Darts.Domain.Game;
using Darts.Domain.Game.Commands;
using Darts.Domain.Game.Entities;
using Darts.Domain.Game.Events;
using Darts.Domain.Infrastructure;
using Xunit;

namespace Darts.Domain.Tests
{
    public class AddPlayerToStartedGame : Specification<Game.Game, AddPlayer>
    {
        protected override Game.Game CreateSubject()
        {
            return Game.Game.CreateNew();
        }

        protected override IEnumerable<Event> Given()
        {
            var id = Identity.Create();    
            yield return new GameCreated(id, GameType.Cricket, DateTime.UtcNow);
            var playerId = Identity.Create();
            yield return new PlayerAdded(Subject.Id, playerId);
            yield return new TurnCompleted(Subject.Id,
                playerId,
                new Turn(playerId,
                    1,
                    new Throw(3, 20),
                    new Throw(3, 20),
                    new Throw(3, 20)));
        }

        protected override IHandle<AddPlayer> OnHandler()
        {
            return new AddPlayerHandler(new FakeRepository<Game.Game> { Subject });
        }

        protected override AddPlayer When()
        {
            return new AddPlayer(Subject.Id, Identity.Create());
        }

        [Fact]
        public void CantAddPlayerToStartedGame_GameAlreadyStartedException()
        {
            Assert.IsType<GameAlreadyStartedException>(Caught);
        }
    }
}