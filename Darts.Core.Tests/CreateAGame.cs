﻿using System.Collections.Generic;
using Darts.Common;
using Darts.Domain.Game.Commands;
using Darts.Domain.Game.Entities;
using Darts.Domain.Game.Events;
using Xunit;

namespace Darts.Domain.Tests
{
    public class CreateAGame : Specification<Game.Game, CreateGame>
    {
        protected override Game.Game CreateSubject()
        {
            return Game.Game.CreateNew();
        }

        protected override IEnumerable<Event> Given()
        {
            return new Event[0];
        }

        protected override IHandle<CreateGame> OnHandler()
        {
            return new CreateGameHandler(new FakeRepository<Game.Game>(),
                () => Subject);
        }

        protected override CreateGame When()
        {
            return new CreateGame(Subject.Id, GameType.Cricket);
        }

        [Fact]
        public void HasAGameCreatedEvent()
        {
            Events.HasSingleOf<GameCreated>();
        }
    }
}