﻿using System;
using System.Collections.Generic;
using Darts.Common;
using Darts.Domain.Game.Commands;
using Darts.Domain.Game.Entities;
using Darts.Domain.Game.Events;
using Darts.Domain.Infrastructure;
using Xunit;

namespace Darts.Domain.Tests
{
    public class AddPlayerToExistingGame : Specification<Game.Game, AddPlayer>
    {
        protected override Game.Game CreateSubject()
        {
            return Game.Game.CreateNew();
        }

        protected override IEnumerable<Event> Given()
        {
            var id = Identity.Create();
            yield return new GameCreated(id, GameType.Cricket, DateTime.UtcNow);
        }

        protected override IHandle<AddPlayer> OnHandler()
        {
            return new AddPlayerHandler(new FakeRepository<Game.Game> { Subject });
        }

        protected override AddPlayer When()
        {
            return new AddPlayer(Subject.Id, Identity.Create());
        }

        [Fact]
        public void PlayerWasAdded()
        {
            Events.Has<PlayerAdded>(1);
        }
    }
}