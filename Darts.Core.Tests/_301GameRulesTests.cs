﻿using System.Collections.Generic;
using System.Linq;
using Darts.Domain.Game.Entities;
using Darts.Domain.Game.Services;
using Darts.Domain.Infrastructure;
using Xunit;

namespace Darts.Domain.Tests
{
    // ReSharper disable once InconsistentNaming
    public class _301GameRulesTests
    {
        private readonly List<Turn> _turns;
        private readonly Identity _identity;
        private readonly _301GameRules _rules;

        public _301GameRulesTests()
        {
            _identity = Identity.Create();
            _turns = new List<Turn>();
            _rules = new _301GameRules();
        }

        private void ProcessTurn(Turn newTurn)
        {
            _rules.ScoreTurn(newTurn, _turns);
            _turns.Add(newTurn);
        }

        [Fact]
        public void FirstTurnWithNoDouble_NoDartsCount()
        {
            var turn = Turn._180(_identity, 1);
            ProcessTurn(turn);

            Assert.Equal(0, turn.Score);
        }

        [Fact]
        public void FirstTurnWithADouble_TwoDartsCount()
        {
            var turn = new Turn(_identity,
                1,
                new Throw(1, 20),
                new Throw(2, 20),
                new Throw(3, 20));

            ProcessTurn(turn);

            Assert.Equal(100, turn.Score);
        }

        [Fact]
        public void FourTurnGame()
        {
            var turn = new Turn(_identity,
                1,
                new Throw(1, 16),
                new Throw(2, 20),
                new Throw(1, 20));
            ProcessTurn(turn);

            // doubles in on second dart
            Assert.Equal(60, turn.Score);

            turn = Turn._180(_identity, 2);
            ProcessTurn(turn);

            // 61 remaining
            Assert.Equal(240, _turns.Sum(t => t.Score));

            turn = new Turn(_identity,
                3,
                new Throw(1, 11),
                new Throw(1, 20),
                new Throw(3, 20));
            ProcessTurn(turn);

            // busted
            Assert.Equal(240, _turns.Sum(t => t.Score));

            turn = new Turn(_identity,
                4,
                new Throw(1, 1),
                new Throw(1, 20),
                new Throw(2, 20));
            ProcessTurn(turn);

            Assert.Equal(301, _turns.Sum(t => t.Score));
            Assert.True(_rules.IsGameCompleted(_identity, _turns));
        }

        [Fact]
        public void ScoreCannotBe1()
        {
            var turn = new Turn(_identity,
                1,
                new Throw(1, 16),
                new Throw(2, 20),
                new Throw(1, 20));
            ProcessTurn(turn);
            // 241 left
            Assert.Equal(60, turn.Score);

            turn = Turn._180(_identity, 2);
            ProcessTurn(turn);

            // 61 left
            Assert.Equal(240, _turns.Sum(t => t.Score));

            turn = new Turn(_identity,
                3,
                new Throw(3, 20),
                new Throw(1, 00),
                new Throw(3, 00));
            ProcessTurn(turn);

            // scored 60, left 1 point on board, that's a bust
            Assert.Equal(240, _turns.Sum(t => t.Score));
        }
    }
}