﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Darts.Domain.Game.Commands;
using Darts.Domain.Game.Entities;
using Darts.Domain.Infrastructure;
using Darts.Domain.Persistence;
using Newtonsoft.Json;
using SqlStreamStore;
using SqlStreamStore.Infrastructure;
using SqlStreamStore.Streams;
using Xunit;

namespace Darts.Domain.Tests.Integration
{
    public class SaveGame
    {
        private readonly StreamStoreBase _store;
        private SqlStreamStoreRepository<Game.Game> _repo;
        private UnitOfWork _uow;
        public SaveGame()
        {
            _store = new InMemoryStreamStore();
            _uow = new UnitOfWork();
            _repo = new SqlStreamStoreRepository<Game.Game>(_store, _uow, Game.Game.CreateNew);
        }

        [Fact]
        public async Task CanSaveNewGameAndLoadFromInMemoryStore()
        {
            async Task Save()
            {
                // get pending changes from unit of work
                var aggregates = _uow.GetChanges()
                    .ToList();

                // save the changes, resulting in a new stream
                foreach (var aggregate in aggregates)
                {
                    var streamId = new StreamId(aggregate.Id.ToString());
                    var newStreamMessages = aggregate.GetUncommittedEvents()
                        .Select(e => new NewStreamMessage(Guid.NewGuid(),
                            e.Type,
                            JsonConvert.SerializeObject(e)))
                        .ToArray();

                    await _store.AppendToStream(
                        streamId,
                        aggregate.Version,
                        newStreamMessages);
                }
            }

            // handle a create game command
            var handler = new CreateGameHandler(_repo, Game.Game.CreateNew);
            var identity = Identity.Create();
            await handler.Handle(new CreateGame(identity, GameType.Cricket));

            await Save();

            // look directly into the store and make sure it saved correctly
            var stream = await _store.ReadStreamForwards(identity.ToString(), 0, 1);
            Assert.Single(stream.Messages);

            // load the aggregate via application infrastructure to make sure it exists
            ResetInfrastructure();
            var fromRepo = await _repo.Get(identity);
            Assert.Equal(identity, fromRepo.Id);
            Assert.Equal(0, fromRepo.Version);

            ResetInfrastructure();
            // add a player
            var handler2 = new AddPlayerHandler(_repo);
            await handler2.Handle(new AddPlayer(identity, Identity.Create()));
            await Save();

            ResetInfrastructure();
            fromRepo = await _repo.Get(identity);
            Assert.Equal(1, fromRepo.Version);
        }

        private void ResetInfrastructure()
        {
            _uow = new UnitOfWork();
            _repo = new SqlStreamStoreRepository<Game.Game>(_store, _uow, Game.Game.CreateNew);
        }
    }
}