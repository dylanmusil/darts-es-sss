﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Darts.Domain.Infrastructure;
using Darts.Domain.Persistence;

namespace Darts.Domain.Tests
{
    internal class FakeRepository<T> : IRepository<T>, IEnumerable<T> where T : Aggregate
    {
        private readonly List<T> _list = new List<T>();

        public void Add(T agg)
        {
            _list.Add(agg);
        }

        public Task<T> Get(Identity aggregateId)
        {
            return Task.FromResult(_list.FirstOrDefault(t => t.Id.Equals(aggregateId)));
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }
    }
}