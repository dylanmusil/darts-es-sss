﻿using System;
using Darts.Common;
using Darts.Domain.Game.Entities;
using Darts.Domain.Game.Events;
using Darts.Domain.Infrastructure;
using Newtonsoft.Json;
using Xunit;

namespace Darts.Domain.Tests
{
    public abstract class Deserialize<T> where T : Event
    {
        protected abstract T Factory();
        protected T Result { get; }
        protected readonly T Subject;
        protected Deserialize()
        {
            // ReSharper disable once VirtualMemberCallInConstructor
            Subject = Factory();
            var json = JsonConvert.SerializeObject(Subject);
            Result = (T)Event.CreateFromJson(json, EventNames.TypeNameToType);
        }
    }

    public class DeserializeGameCreated : Deserialize<GameCreated>
    {
        protected override GameCreated Factory()
        {
            return new GameCreated(Identity.Create(), GameType.Cricket, DateTime.UtcNow);
        }

        [Fact]
        public void GameIdsShouldMatch()
        {
            Assert.Equal(Subject.Id, Result.Id);
        }

        [Fact]
        public void GameTypesShouldMatch()
        {
            Assert.Equal(Subject.GameType, Result.GameType);
        }

        [Fact]
        public void CreatedDateShouldMatch()
        {
            Assert.Equal(Subject.Created, Result.Created);
        }
    }

    public class DeserializeGameCompleted : Deserialize<GameCompleted>
    {
        protected override GameCompleted Factory()
        {
            return new GameCompleted(Identity.Create(), Identity.Create());
        }

        [Fact]
        public void GameIdsShouldMatch()
        {
            Assert.Equal(Subject.Id, Result.Id);
        }

        [Fact]
        public void GameWinnerShouldMatch()
        {
            Assert.Equal(Subject.Winner, Result.Winner);
        }
    }

    public class DeserializePlayerAdded : Deserialize<PlayerAdded>
    {
        protected override PlayerAdded Factory()
        {
            return new PlayerAdded(Identity.Create(), Identity.Create());
        }

        [Fact]
        public void GameIdsShouldMatch()
        {
            Assert.Equal(Subject.Id, Result.Id);
        }

        [Fact]
        public void PlayerIdsShouldMatch()
        {
            Assert.Equal(Subject.PlayerId, Result.PlayerId);
        }
    }

    public class DeserializeTurnCompleted : Deserialize<TurnCompleted>
    {
        protected override TurnCompleted Factory()
        {
            var playerId = Identity.Create();
            var turn = new Turn(playerId, 1, new Throw(3, 20), null, null);
            return new TurnCompleted(Identity.Create(), playerId, turn);
        }

        [Fact]
        public void GameIdsShouldMatch()
        {
            Assert.Equal(Subject.Id, Result.Id);
        }

        [Fact]
        public void PlayerIdsShouldMatch()
        {
            Assert.Equal(Subject.PlayerId, Result.PlayerId);
        }

        [Fact]
        public void FirstTurnsShouldMatch()
        {
            Assert.Equal(Subject.Turn, Result.Turn);
        }
    }
}