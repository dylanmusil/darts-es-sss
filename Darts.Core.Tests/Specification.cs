﻿using System;
using System.Collections.Generic;
using Darts.Common;
using Darts.Domain.Infrastructure;

// ReSharper disable VirtualMemberCallInConstructor

namespace Darts.Domain.Tests
{
    public abstract class Specification<T, TCommand> where T : Aggregate where TCommand : Command
    {
        protected abstract T CreateSubject();
        protected abstract IEnumerable<Event> Given();
        protected abstract IHandle<TCommand> OnHandler();
        protected abstract TCommand When();
        protected readonly Exception Caught;
        protected readonly T Subject;
        protected Specification()
        {
            try
            {
                Subject = CreateSubject();
                Subject.LoadEvents(Given());
                OnHandler().Handle(When()).GetAwaiter().GetResult();
                Events = Subject.GetUncommittedEvents();
            }
            catch (Exception e)
            {
                Caught = e;
            }
        }

        protected IEnumerable<Event> Events { get; }
    }
}