using System;
using System.Linq;
using Darts.Common;
using Darts.Domain.Game.Entities;
using Darts.Domain.Game.Events;
using Darts.Domain.Infrastructure;
using Xunit;

namespace Darts.Domain.Tests
{
    public class RouterTests
    {
        private class MyTestEvent : Event
        {
        }

        [Fact]
        public void CanRegisterHandler()
        {
            var router = new Router();

            router.Register((MyTestEvent e)=> {});
        }

        [Fact]
        public void CantRegisterTwoHandlersForSameEvent()
        {
            var router = new Router();
            var handler = new Action<MyTestEvent>(e => { });

            router.Register(handler);
            Assert.Throws<HandlerAlreadyRegisteredException>(() => router.Register(handler));
        }
    }

    public class AggregateTests
    {
        [Fact]
        public void CreateGame_ShouldPopulateCreated()
        {
            var game = new Game.Game();
            game.Create(Identity.Create(), GameType.Cricket);

            Assert.Single(game.GetUncommittedEvents());
            Assert.IsType<GameCreated>(game.GetUncommittedEvents()
                .Single());
        }

        [Fact]
        public void CreateGameAndAddTwoPlayers()
        {
            var game = new Game.Game();
            game.Create(Identity.Create(), GameType.Cricket);
            game.AddPlayer(Identity.Create());
            game.AddPlayer(Identity.Create());

            Assert.Equal(3, game.GetUncommittedEvents().Count());
            Assert.IsType<GameCreated>(game.GetUncommittedEvents().First());
            Assert.IsType<PlayerAdded>(game.GetUncommittedEvents().ToArray()[1]);
            Assert.IsType<PlayerAdded>(game.GetUncommittedEvents().ToArray()[2]);
        }
    }
}
