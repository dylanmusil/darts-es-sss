using System.Threading.Tasks;
using Darts.Common;
using Darts.Domain.Game.Commands;
using Darts.Domain.Game.Entities;
using Darts.Domain.Infrastructure;
using Darts.Games.Views;
using Darts.Games.Views.Projections;
using Darts.Games.Views.Projections.GameList;
using Darts.Write.API.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using SqlStreamStore.Infrastructure;
using SqlStreamStore.Streams;
using GameType = Darts.Domain.Game.Entities.GameType;

namespace Darts.Integration
{
    /// <summary>
    /// Simulates two systems that communicate asynchronously.
    /// </summary>
    public class BusTests
    {
        /// <summary>
        /// Provides services for the write side.
        /// </summary>
        private readonly ServiceProvider _writeProvider;
        /// <summary>
        /// Provides services for the read side.
        /// </summary>
        private readonly ServiceProvider _readProvider;

        public BusTests()
        {
            _writeProvider = new ServiceCollection()
                .AddScoped<Mediator>()
                .RegisterCommandHandlers()
                .BuildServiceProvider();

            _readProvider = new ServiceCollection()
                .AddScoped<Mediator>()
                .RegisterReadHandlers(true)
                .BuildServiceProvider();

            _readProvider.GetService<DartsViewContext>().Database.EnsureDeleted();

        }

        [Fact]
        public async Task CanSendCreateGame()
        {
            var bus = _writeProvider.GetService<ISendCommands>();
            await bus.Send(new CreateGame(Identity.Create(), GameType.Cricket));
        }

        /// <summary>
        /// Use the write side to process commands.
        /// Events are emitted for a new Game stream.
        /// Capture them on the read side and process them.
        ///
        /// Verifies a full end to end work flow from command
        /// submission to read model projection as it would happen
        /// based on commands sent from a client.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task AddPlayersToANewGameAndStartPlaying()
        {
            var identity = Identity.Create();
            _writeProvider.GetService<StreamStoreBase>()
                .SubscribeToStream(new StreamId(identity.ToString()),
                    null, async (subscription, message, token) =>
                    {
                        var @event = Event.CreateFromJson(await message.GetJsonData(token),
                            ReadEventNames.TypeNameToType);
                        if (@event != null)
                        {
                            using (var scope = _readProvider.CreateScope())
                            {
                                var bus = scope.ServiceProvider.GetService<INotifyOfEvents>();
                                await bus.Notify(@event);
                                var readDb = scope.ServiceProvider.GetService<DartsViewContext>();
                                await readDb.SaveChangesAsync(token);
                            }
                        }

                    });
            using (var scope = _writeProvider.CreateScope())
            {
                var bus = scope.ServiceProvider.GetService<ISendCommands>();
                await bus.Send(new CreateGame(identity, GameType.Single501));
            }

            var player1 = Identity.Create();
            using (var scope = _writeProvider.CreateScope())
            {
                var bus = scope.ServiceProvider.GetService<ISendCommands>();
                await bus.Send(new AddPlayer(identity, player1));
            }

            var player2 = Identity.Create();
            using (var scope = _writeProvider.CreateScope())
            {
                var bus = scope.ServiceProvider.GetService<ISendCommands>();
                await bus.Send(new AddPlayer(identity, player2));
            }

            using (var scope = _writeProvider.CreateScope())
            {
                var bus = scope.ServiceProvider.GetService<ISendCommands>();
                await bus.Send(new SubmitTurn(identity, player1, Turn._180(player1, 1)));
            }

            using (var scope = _writeProvider.CreateScope())
            {
                var bus = scope.ServiceProvider.GetService<ISendCommands>();
                await bus.Send(new SubmitTurn(identity, player2, Turn._180(player2, 1)));
            }

            // subscriptions are EC
            await Task.Delay(100);

            // time to start reading so switch providers
            using (var scope = _readProvider.CreateScope())
            {
                var bus = scope.ServiceProvider.GetService<IQuery>();
                var result = await bus.Query<GameListQueries.ForPlayer, GameListResult>(new GameListQueries.ForPlayer(player1));
                Assert.NotNull(result);
                var game = Assert.Single(result.Games);
                Assert.NotNull(game);
                Assert.Equal(player1, game.PlayerId);
            }

            using (var scope = _readProvider.CreateScope())
            {
                var bus = scope.ServiceProvider.GetService<IQuery>();
                var result = await bus.Query<GameListQueries.ForPlayer, GameListResult>(new GameListQueries.ForPlayer(player2));
                Assert.NotNull(result);
                var game = Assert.Single(result.Games);
                Assert.NotNull(game);
                Assert.Equal(player2, game.PlayerId);
            }
        }
    }
}
