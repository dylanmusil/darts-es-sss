﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Darts.Common;
using Darts.Domain.Game;
using Darts.Domain.Game.Commands;
using Darts.Domain.Game.Entities;
using Darts.Domain.Infrastructure;
using Darts.Domain.Persistence;
using Darts.Write.API;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using Xunit;

namespace Darts.Integration
{
    public class CommandApiTest
    {
        private readonly HttpClient _client;
        private readonly SqlStreamStoreRepository<Game> _repository;

        public CommandApiTest()
        {
            var builder = new WebHostBuilder().UseStartup<Startup>()
                // in-memory storage is only available in integration
                .UseEnvironment("Integration");
            var server = new TestServer(builder);
            _repository =
                (SqlStreamStoreRepository<Game>)
                server.Host.Services.GetService(
                    typeof(SqlStreamStoreRepository<Game>));
            _client = server.CreateClient();
        }

        private async Task PostCommand(Command c)
        {
            var response = await _client.PostAsync("api/commands",
                new StringContent(JsonConvert.SerializeObject(c),
                    Encoding.UTF8,
                    "application/json"));
            Assert.Equal(HttpStatusCode.Accepted, response.StatusCode);
        }

        [Fact]
        public async Task PlayAGame()
        {
            var gameId = Identity.Create();
            Command command = new CreateGame(gameId, GameType.Single501);
            await PostCommand(command); // 0

            var playerId = Identity.Create();
            command = new AddPlayer(gameId, playerId);
            await PostCommand(command); // 1

            command = new SubmitTurn(gameId, playerId, Turn._180(playerId, 1));
            await PostCommand(command); // 2
            command = new SubmitTurn(gameId, playerId, Turn._180(playerId, 2));
            await PostCommand(command); // 3
            command = new SubmitTurn(gameId,
                playerId,
                new Turn(playerId,
                    3,
                    new Throw(3, 20),
                    new Throw(3, 19),
                    new Throw(2, 12)));
            await PostCommand(command);

            var game = await _repository.Get(gameId);
            Assert.Equal(5, game.Version); // 2 events recorded when a turn causes completion
        }
    }
}